import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

// import { AppModule } from './app/app.module';
// import { AppModule } from './app/routing/app.module';
import { AppModule } from './app/lifecycle/app.module';

platformBrowserDynamic().bootstrapModule(AppModule);
