import { Component } from '@angular/core'

@Component({
  selector: 'event-app',
  styles : [
      `h1 {color:green;}`
  ],
  template: `<h1>{{name}}</h1>
             <h2>{{email}}</h2>
             <h3>{{city}}</h3>
             <h4>{{state}}</h4>`
})
export class EmployeeComponent1
{
name='Anagha Unnikrishnan';
email='anagha@ust-global.com';
city='kochi';
state='Kerala';
}