import  {  Component , OnInit  }  from  '@angular/core';
import  {  EmployeeService  }  from  './employee.service';

@Component({
    selector:  'employee-detail',
    templateUrl:  '/app/employeedetail.componet.html',
})

export  class  EmployeeDetailComponent  implements  OnInit {
    employees :  Array<any>;

    constructor(private  _empDetails:  EmployeeService)
    { }
    //allData is a local var and ngOnInit is called after constructor
    ngOnInit() {
        this._empDetails.getEmployees().subscribe(allData  =>  this.employees  =  allData);
    }
} 