import { NgModule }      from '@angular/core';

import { MyHiddenDirective } from './myHidden.directive';

@NgModule({
  declarations:   [ MyHiddenDirective ],
  exports:        [ MyHiddenDirective ]
})
export class SharedModule { }