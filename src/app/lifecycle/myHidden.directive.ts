import { Directive, ElementRef,  Renderer , Input } from '@angular/core';

@Directive({
  selector: '[myHidden]'
})

export class MyHiddenDirective {

  // constructor(_renderer: Renderer,_elementRef: ElementRef) { 
      // _renderer.setElementStyle(_elementRef.nativeElement,'display','none');
  constructor(public _renderer: Renderer,public _elementRef: ElementRef) { 
  } 

  @Input()
  myHidden:boolean;

  ngOnInit(){
    if(this.myHidden){
        this._renderer.setElementStyle(this._elementRef.nativeElement,'display','none');
    }
  }
  
}