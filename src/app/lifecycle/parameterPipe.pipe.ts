import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
   name: 'formatMobileNumber'
})

export class FormatMobileNumberPipe implements PipeTransform
{
    // transform(value: string, before: string, after : string):string{
     transform(value: string, format: string):string{

        return value + ' <=name UstGlobal Format =>: '+ format;
    }
      
}
 