import { Component } from '@angular/core';
import { UserComponent } from './user.component';

@Component({
 selector: 'my-app',
  template: `<h1>Employee Component</h1>
                        <user-form> [valueToChange] = "objectToChange">   </user-form>
            `
})
export class EmployeeComponent
{
    numberToChange = 90;
    objectToChange = 
    {
        numberToChange : this.numberToChange
    }
 
}
 