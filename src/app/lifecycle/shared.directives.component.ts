import { NgModule }      from '@angular/core';

import { MyHiddenDirective } from './myHidden.directive';

@NgModule({
  imports:      [ MyHiddenDirective ],
  exports:    [ MyHiddenDirective ]
})
export class SharedModule { }