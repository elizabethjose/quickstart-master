import { NgModule , Component}      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { UserComponent } from './user.component';
import { EmployeeComponent } from './employee.component';
import { ReverseString } from './reverse-str.pipe';
import { FormatMobileNumberPipe } from './parameterPipe.pipe';
import { SharedModule } from './sharedDirectives.component';

@NgModule({
  imports:      [ BrowserModule , SharedModule ],
  declarations: [ UserComponent, EmployeeComponent, ReverseString, FormatMobileNumberPipe ],
  bootstrap:    [ EmployeeComponent ]
})
export class AppModule { }