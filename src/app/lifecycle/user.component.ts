
import { Component,Input,OnInit,OnChanges,OnDestroy,AfterContentInit } from '@angular/core';

@Component({
    selector: 'user-form',
    templateUrl: './user.components.html',
})

export class UserComponent implements OnInit,OnChanges,OnDestroy,AfterContentInit
{
    @Input()
    valueToChange :string;
    today = new Date();
    name :string;
    email :string;
    val : boolean = true; // My hidden componet Hide enabled
    // val : boolean = false;  // My hidden componet Hide disenabled

    constructor(){
        console.log("user constructor called");
        this.name="Ahmed";
        this.email="tufailahmedkhan@gmail.com";
    }
    ngOnInit(){ console.log("ng ngOnInitCalled() called"); }
    ngOnChanges(){ console.log("ng ngOnChangesCalled() called"); }
    ngOnDestroy(){ console.log("ng ngOnDestroyCalled() called"); }
    ngAfterContentInit() { console.log("ng gAfterContentInit() called"); }
}
 