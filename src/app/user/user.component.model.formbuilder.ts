import  {  Component , OnInit  }  from  '@angular/core';
import { User } from './user';
import { FormGroup , FormControl , Validators, FormBuilder  } from '@angular/forms';


@Component({
    selector:  'user-detail-fb',
    // templateUrl:  '/app/user/user.component.html',
    templateUrl:  '/app/user/user.component.model.formbuilder.html',
})

export  class  UserComponentMFB {

    userForm4 : FormGroup;
    
    constructor(private _formBuilder:FormBuilder){}

    ngOnInit(){
        this.userForm4 = this._formBuilder.group({
            name:[null , Validators.minLength(5) ],
            email:'',
            mobile: '',
            address: this._formBuilder.group({
                city:'',
                state: '',
                zip: ''
            })
        })
    }
    

    onSubmit(){
        console.log(this.userForm4.value);
    }

}