export class User {

  constructor(
    public name: string,
    public email: string,
   public city: string,
   public state: string,
   public zip: string
  ) {  }

}