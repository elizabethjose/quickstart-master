import  {  Component , OnInit  }  from  '@angular/core';
import { User } from './user';
import { FormGroup , FormControl , Validators  } from '@angular/forms';


@Component({
    selector:  'user-detail-model',
    // templateUrl:  '/app/user/user.component.html',
    templateUrl:  '/app/user/user.component.model.driven.form.html',
})

export  class  UserComponentModelDriven {

    userForm2 = new FormGroup({

        nameModel : new FormControl(null , [Validators.required , Validators.minLength(5)] ),
        emailModel : new FormControl(null , Validators.email ),

        addressModel : new FormGroup({
            cityModel : new FormControl(),
            stateModel : new FormControl(),
            zipModel : new FormControl(null,Validators.pattern("^[P][4-9]{4}") ),
        })

    })

    onSubmit(){
        console.log(this.userForm2.value);
    }

}