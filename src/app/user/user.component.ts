import  {  Component   }  from  '@angular/core';
import { User } from './user';

@Component({
    selector:  'user-detail',
    // templateUrl:  '/app/user/user.component.html',
    templateUrl:  '/app/user/user.component.1.html',
})

export  class  UserComponent {

    // model = new User('Name','x@x.com','City','State','E1868');

    onSubmit(userForm:any){
        console.log(userForm);
    }

    constructor(){
        console.log("constructor");
    }

    ngOnInit(){
        console.log("ngOnInit");
    }

    ngOnChange(){
        console.log("ngOnChange");
    }


}