import { Component , OnInit } from '@angular/core';
import { EmployeeService } from './employee.service';



@Component({
    selector: 'employee-list',
    templateUrl: '/app/employeelist.component.html',
})

export class EmployeeListComponent implements OnInit {
    employees : Array<any>;
    errorList: string;
    constructor(private _empService : EmployeeService ) {
    }

    ngOnInit() {
        this._empService.getEmployees().subscribe(
            data => this.employees = data, err => this.errorList = err);
    }

} 