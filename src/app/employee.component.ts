
import { Component } from '@angular/core';
import {EmployeeService} from './employee.service';


@Component({
    selector: 'my-employee',
    template: `
    <h2> UST Global List</h2>
    <employee-list> </employee-list>
    <employee-detail> </employee-detail>

    `,
    providers : [EmployeeService]
})

export class EmployeeComponent 
{
} 
