import { Injectable ,Component } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';



@Injectable()
// @Component({})
export class EmployeeService {


  private _url:string = "/employeedata/empinfo.json";
  // private _url: string = "https://jsonplaceholder.typicode.com/postsxxxxxxxxx";

  constructor(private _http: Http) { }

  getEmployees() {
    return this._http.get(this._url)
      .map((resp: Response) => resp.json())
      .catch(this.handleError);;
  }

  private handleError (error: Response) {
    // In a real world app, you might use a remote logging infrastructure
/*    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);*/

 console.error(error);
    return Observable.throw(error);

  }

}