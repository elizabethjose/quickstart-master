import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// import { AppComponent }  from './app.component';

import { AppComponent }  from './routing/app.component';
import { EmployeeComponent } from './employee.component';

import { SimpleNgModelComp } from './manager.component';
import { HeroParentComponent } from './hero-parent.component';
import { HeroChildComponent } from './hero-child.component';
import {EmployeeListComponent} from './employeelist.component';
import {EmployeeDetailComponent} from './employeedetail.componet';
// import { UserComponent} from './user/user.component';
import { UserComponent} from './user/user.component.1';
import { UserComponentModelDriven} from './user/user.component.model.driven.form';
import { UserComponentMFB} from './user/user.component.model.formbuilder';

@NgModule({
  imports:      [ BrowserModule,FormsModule,
        HttpModule, ReactiveFormsModule ],
        declarations: [ AppComponent,EmployeeComponent, SimpleNgModelComp ,
        HeroParentComponent, HeroChildComponent, EmployeeListComponent,EmployeeDetailComponent,
        EmployeeComponent,  UserComponent,  UserComponentModelDriven,  UserComponentMFB],
 /* bootstrap:    [ AppComponent,EmployeeComponent,SimpleNgModelComp ,
  HeroParentComponent, HeroChildComponent,EmployeeListComponent,EmployeeDetailComponent,
  EmployeeComponent]
  */
  bootstrap: [ UserComponent  ]
})
export class AppModule { }
