import {Component} from '@angular/core';
 
@Component({
  selector: 'example-app',
  template: `
    <input [(ngModel)]="name" #ctrl="ngModel" required>
 
    <p>Value: {{ name }}</p>
    <p>Valid: {{ ctrl.valid }}</p> <p *ngIf="ctrl.valid" >Text to show</p>
    
    <button (click)="setValue()">Set value</button>
  `,
})
export class SimpleNgModelComp {
  name: string = '';
  imageLink : string ;

 
  setValue() { console.log("my method called") ;}

  constructor(){
        this.name = 'ELiza';
        console.log("Constructor method called") ;
  }
}