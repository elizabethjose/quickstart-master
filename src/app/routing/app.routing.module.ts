
import { NgModule }      from '@angular/core';
import { RouterModule , Routes } from '@angular/router';

import { EmployeeListComponent } from './employee-list.component';
import { DepartmentListComponent } from './department-list.component';
import { ProjectListComponent } from './project-list.component';
import { DepartmentDetailComponent } from './department-detail.component';
import { PageNotFoundComponent } from './page-not-found.component';

const routes:Routes = [
            // Homepage
            // { path: '' , component : DepartmentListComponent },
            // Anothewr way of Homepage
            { path: '' , redirectTo : '/employees' , pathMatch :'full'},
            // Go to empty string /departments; It will display first component. All others wont work.
            // will search /employees/departments
            // { path: '' , redirectTo : '/employees' , pathMatch :'prefix'},
            { path: 'employees' , component : EmployeeListComponent },
            // { path: 'departments' , component : DepartmentListComponent } , 
            { path: 'departmentsList' , component : DepartmentListComponent } , 
            // { path: 'departments/:id/:name' , component : DepartmentDetailComponent } , 
            { path: 'departmentsList/:id' , component : DepartmentDetailComponent } , 
            { path: 'projects' , component : ProjectListComponent } ,
            { path: '**', component: PageNotFoundComponent }
          ]


@NgModule({
      imports : [ RouterModule.forRoot(routes) ]  ,
      exports : [ RouterModule]
})

export class AppRoutingModule { }

export const routingComponents = [ EmployeeListComponent ,                                   
                                  ProjectListComponent,
                                  DepartmentListComponent,
                                  DepartmentDetailComponent,
                                  PageNotFoundComponent
                                  ] 