import { Component , OnInit} from '@angular/core';
import { ActivatedRoute , Router , Params } from '@angular/router';


@Component({
    template: `<h3>You selected department with ID = {{departmentId}} and NAME = {{departmentName}}</h3>
             
                <button (click) = "goPrevious()">Previous</button>
                <button (click) = "goNext()">Next</button>
                <button (click) = "goBack()">Back</button>
    `,
    })
export class DepartmentDetailComponent
{
    departmentId:number;
    departmentName: string;

    constructor(private _activatedRoute:ActivatedRoute, private _router:Router ){}

/*    ngOnInit(){
        let id = this._activatedRoute.snapshot.params['id'];
        this.departmentId = id ;
        let name = this._activatedRoute.snapshot.params['name'];
        this.departmentName = name ;
        console.log("ngOnInit called!")
    }*/

    ngOnInit(){
        this._activatedRoute.params.subscribe((params: Params) =>{
            let id = parseInt(params['id']);
            this.departmentId = id ;
            let name = params['name'];
            this.departmentName = name ;     
             console.log("this.departmentId ::"+this.departmentId+" this.departmentName :: "+this.departmentName); 
        })
           console.log("ngOnInit called!");
    }

    goPrevious(){
        let prevId =  this.departmentId - 1 ;
        // this._router.navigate(['/departments', prevId])
        // this._router.navigate(['/departments', --this.departmentId])

        this._router.navigate(['../', {id:prevId}], { relativeTo: this._activatedRoute });
    }

    goNext(){
        let nextId =  this.departmentId + 1 ;
        // this._router.navigate(['/departments', nextId])
        // this._router.navigate(['/departments', ++this.departmentId])

        this._router.navigate(['../',  {id:nextId}], { relativeTo: this._activatedRoute });
    }

    goBack(){
            let selectedId = this.departmentId ? this.departmentId : null;
            let selectedName = this.departmentName ? this.departmentName : null;

    // Pass along the hero id if available
    // so that the HeroList component can select that hero.
    // Include a junk 'foo' property for fun.
            // this._router.navigate(['/departments', { id: selectedId, name: selectedName }]);
        
            // this._router.navigate(['/departments', {id: selectedId}]);
       

            this._router.navigate(['../', {id: selectedId}], { relativeTo: this._activatedRoute });

    }
}
