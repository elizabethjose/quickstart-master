import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `<h3>Routing Application</h3>

              <nav>
                <a routerLink='/employees' routerLinkActive="active"> EMP LIST</a> |
                <a routerLink='/departmentsList' routerLinkActive="active" > DEPT LIST</a> |
                <a routerLink='/projects'routerLinkActive="active"> PROJECT LIST </a>
              </nav>

              <router-outlet>    </router-outlet>
   
              `,
})
export class AppComponent 
{
    public childData: string;
}
