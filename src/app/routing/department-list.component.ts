import { Component , OnInit ,OnDestroy } from '@angular/core'; 
import { Router , ActivatedRoute , Params } from '@angular/router'; 

@Component({
  selector: 'department-list',
  template: `<h3>Department List </h3>

              <ul class="items">

                <li *ngFor="let department of departments" 
                  [class.selected]="isSelected(department)"                  
                    (click)="onSelect(department)">
              
                     <span class="badge">  {{department.id}} </span>
                      - {{department.name}}
                </li>
              </ul>           
            `,
})
export class DepartmentListComponent implements OnInit, OnDestroy{

  selectedId : number;
  constructor(private _router:Router, private _activatedRoute: ActivatedRoute){  }

 

  departments = [
                  { id: "1", name: "HR" },
                  { id: "2", name: "Finance" },
                  { id: "3", name: "BackOffice" },
                  { id: "4", name: "FrontOffice" },
                  { id: "5", name: "Marketing" },
                ]


 ngOnInit(){
        this._activatedRoute.params.subscribe((params: Params) =>{
            let id = parseInt(params['id']);
            this.selectedId = id ;
            // let name = params['name'];
            // this.departmentName = name ;     
            //  console.log("this.departmentId ::"+this.departmentId+" this.departmentName :: "+this.departmentName); 
             console.log("this.selectedId ::"+this.selectedId); 
        })
           console.log("ngOnInit called!");
    }


/*  onSelect(department:any){
    this._router.navigate(['departments', department.id , department.name ])
  }*/

    onSelect(department:any){
      this._router.navigate([department.id], { relativeTo: this._activatedRoute })
    }

    isSelected(dept:any){
      return dept.id == this.selectedId;
    }

    ngOnDestroy(){
      
    }
}

